################################################################################
# Make file for Scheduler
################################################################################

SHELL = /bin/sh

GNU  	= g++-4.9
CLANG = clang++

FLAGS   = --std=c++11 -pthread -O2

################################################################################
# Rules
################################################################################

gnu:	scheduler_gnu

clang:	scheduler_clang

default: gnu
all: gnu clang

scheduler_gnu: src/main.cpp
	$(GNU) $(FLAGS) -O src/main.cpp -o build/scheduler_gnu.o

scheduler_clang: src/main.cpp
	$(CLANG) $(FLAGS) -O src/main.cpp -o build/scheduler_clang.o

clean:
	rm -rf build/*
