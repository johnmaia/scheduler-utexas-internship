#include <iostream>
#include <unistd.h>
#include <cstdlib>
#include <pthread.h>
#include "Timer.h"
#include "BlockOfTasks.h"

using namespace std;

class AbstractWorker
{
	public:
		 virtual int getLatency(void) = 0;		 										// Get worker's latency
		 virtual int getThroughput(void) = 0;	 										// Get worker's throughput
		 virtual int getBlockOfTasksSize(void) = 0;	 							// Get worker's Block of Tasks Size
		 virtual int getType(void) = 0;														// Get worker's Type
		 virtual double getElapsedTime(void) = 0;									// Get worker's elapsed time on the last Task
		 virtual int tossCoin(void) = 0;													// Worker toss coin in order to simulate possible latency increase
		 virtual void setElapsedTime(double et) = 0;							// Set worker's elapsed time on the last Task
		 virtual void setBlockOfTasks(BlockOfTasks* bot) = 0;			// Set worker's block of tasks
		 virtual void startWorker() = 0;													// Start worker (pThread)
		 virtual void joinWorker() = 0;														// Join worker (pThread)
		 virtual void freeBlockOfTasks() = 0;											// Free worker's block of tasks
};


template<int>
class Worker : public AbstractWorker
{
   public:
		 	int getLatency(void);		 											// Get worker's latency
		 	int getThroughput(void);	 										// Get worker's throughput
			int getBlockOfTasksSize(void);	 							// Get worker's Block of Tasks Size
			int getType(void);	 													// Get worker's Type
			int tossCoin(void);														// Worker toss coin in order to simulate possible latency increase
			double getElapsedTime(void);									// Get worker's elapsed time on the last Task
			void setElapsedTime(double et);								// Set worker's elapsed time on the last Task
			void setBlockOfTasks(BlockOfTasks* bot);			// Set worker's block of tasks
			void freeBlockOfTasks();											// Free worker's block of tasks
			void startWorker();														// Start worker (pThread)
			void joinWorker();														// Join worker (pThread)
			Worker();				 				 											// Worker constructor

		private:
			//Functions
			static void* threadHandler(void* arg);				// Worker thread handler
			void doTask(void);														// Worker do tasks function

			// Variables
			double elapsedTime;						// Worker's elapsed time on last work
			int	type;		 									// Worker's type
			int	latency;		 							// Worker's initial latency
			int throughput;  	 						// Worker's debit/cycle
			int capacity;									// Worker's tasks capactiy
			int maxCapacityPenalty;				// Worker's tasks penalty/task in excess
			int timeCostPerCycle; 				// Worker's cost per cycle
			int timeCostPerTaskTransfer; 	// Worker's cost per task transfer
			int	loadCost;		 							// Worker's load cost
			int	storeCost;		 						// Worker's store cost
			int	instCost;		 							// Worker's Instruction cost
			BlockOfTasks BoTasks;					// Worker's Tasks
			pthread_t workerThread;				// Worker's pThreads
};


// Worker constructor - Define its latency, throughput, capactiy, etc..
template<int d>
Worker<d>::Worker(void)
{
    latency									= rand() % 4000000 + 500000; 	// Latency (microseconds) - random value 500000-4000000
		throughput							= rand() % 200 + 50; 					// Device debit/cycle - random value 50-200
		timeCostPerCycle				= rand() % 400 + 100; 				// Time cost per cycle (microseconds) - random value 100-400
		capacity								= rand() % 8000 + 6500;				// Workers capacity limit (tasks) - random value 8000-145000
		maxCapacityPenalty			=	rand() % 800 + 650;					// Workers capacity limit (tasks) - random value 800-1450
		type										= -1;
}

// CPU
template<>
Worker<0>::Worker(void)
{
    latency									= rand() % 50 + 40;		 		// Latency (microseconds) - random value 400-900
		throughput							= 64;								 			// Device debit/cycle - random value 70-80
		timeCostPerCycle				= rand() % 40 + 10; 			// Time cost per cycle (microseconds) - random value 40-50
		timeCostPerTaskTransfer	= 0;											// Workers task transfer costs (microseconds)
		loadCost								= 10;
		storeCost								= 10;
		instCost								= 10;
		type										= 0;											// Workers type (0: CPU | 1: GPU | 2: MIC)
}

// GPU
template<>
Worker<1>::Worker(void)
{
    latency									= rand() % 50000 + 40000; 		// Latency (microseconds) - random value 50000-90000
		throughput							= 128; 												// Device debit/cycle - random value 3000-4000
		timeCostPerCycle				= rand() % 15 + 5; 						// Time cost per cycle (microseconds) - random value 15-20
		timeCostPerTaskTransfer	= 1000;												// Workers task transfer costs (microseconds)
		loadCost								= 10;
		storeCost								= 10;
		instCost								= 100;
		type										= 1;													// Workers type (0: CPU | 1: GPU | 2: MIC)
}

// MIC
template<>
Worker<2>::Worker(void)
{
    latency									= rand() % 50000 + 40000; 		// Latency (microseconds) - random value 50000-90000
		throughput							= 512;									 			// Device debit/cycle - random value 3000-4000
		timeCostPerCycle				= rand() % 15 + 5; 						// Time cost per cycle (microseconds) - random value 15-20
		timeCostPerTaskTransfer	= 1000;												// Workers task transfer costs (microseconds)
		loadCost								= 10;
		storeCost								= 10;
		instCost								= 50;
		type										= 2;													// Workers type (0: CPU | 1: GPU | 2: MIC)
}

// Set worker's task
template<int d>
void Worker<d>::setBlockOfTasks(BlockOfTasks* bot){
	// Allocates space for the new block of tasks
	for(unsigned int i = 0; i < bot->getBlockSize(); ++i){
		BoTasks.setTask(bot->getTask(i));
	}
}

// Free worker's block of tasks
template<int d>
void Worker<d>::freeBlockOfTasks(void){
	BoTasks.freeBlock();
}

// Set worker's elapesed time on the last task
template<int d>
void Worker<d>::setElapsedTime(double et){
	elapsedTime = et;
}

//Get worker's Block of Tasks Size
template<int d>
int Worker<d>::getBlockOfTasksSize(void){
	return BoTasks.getBlockSize();
}

// Get worker's latency
template<int d>
int Worker<d>::getLatency(void){
	return latency;
}

// Get worker's throughput
template<int d>
int Worker<d>::getThroughput(void){
	return throughput;
}

// Get worker's elapsed time on the last Task
template<int d>
double Worker<d>::getElapsedTime(void){
	return elapsedTime;
}

// Get worker's elapsed time on the last Task
template<int d>
int Worker<d>::getType(void){
	return type;
}

// Get worker's elapsed time on the last Task
template<int d>
int Worker<d>::tossCoin(void){
	int coin	=	rand() % 100;
	if(coin > 93) return 1;
	else return 0;
}

// Worker do tasks function
template<int d>
void Worker<d>::doTask(void){
	unsigned int latencyPeack = 0;
	float latencyPeackFactor = 0;

	// If array of task has a size, then work it
	if(BoTasks.getBlockSize()){

		//toss a coin and decide if there is a lantency increase or not
		if(tossCoin()){
			latencyPeackFactor = (rand() % 15) + 10;
			latencyPeackFactor /= 10;
			latencyPeack = latency * latencyPeackFactor;
			cout << "[!][!] Latency Peack! | Normal latency: " << latency << " | latencyPeackFactor: " << latencyPeackFactor << " | latencyPeack: " << latencyPeack << endl;
			usleep(latencyPeack);
		} else usleep(latency);

		// Simulate cost of transfer
		usleep((timeCostPerTaskTransfer * BoTasks.getBlockSize()));

		// Iterate through all the tasks
		for(unsigned int i = 0; i < BoTasks.getBlockSize(); ++i){
			// Consume task weight
			for(unsigned int x = 0; x < BoTasks.getTaskWeight(i); ++x){
				x += throughput;
				usleep(timeCostPerCycle);
			}
			// Simulate cost of loads
			usleep((loadCost * BoTasks.getTaskNLoads(i)));

			// Simulate cost of stores
			usleep((storeCost * BoTasks.getTaskNStores(i)));

			// Simulate cost of Instructions
			usleep((instCost * BoTasks.getTaskNInst(i)));

			// Simulate cost of Branches
			usleep((instCost * (BoTasks.getTaskPercentBranch(i)/100)));

			// Simulate cost of cache misses
			usleep((loadCost * (BoTasks.getTaskPercentCacheMiss(i)/100)));
		}
	}
}

template<int d>
void Worker<d>::startWorker(){
	pthread_create(&workerThread, 0, threadHandler, this);
}

template<int d>
void Worker<d>::joinWorker(){
	pthread_join(workerThread,	NULL);
}

template<int d>
void* Worker<d>::threadHandler(void* arg){
	double start, finish, elapsed;
  Worker* w = reinterpret_cast<Worker*>(arg);

	GET_TIME(start);
  w->doTask();
	GET_TIME(finish);

	elapsed = finish - start;
	w->setElapsedTime(elapsed);

	return NULL;
}
