#include <cstdlib>
using namespace std;

class Task
{
   public:
      void setTask(void);             // Set task's info
      int getWeight(void);            // Get task's weight
			int getType(void);		 		      // Get task's Type
      int getNLoads(void);            // Get Number of Loads
      int getNStores(void);           // Get Number of Stores
      int getNInst(void);             // Get Number of Instructions
      int getPercentBranch(void);     // Get Percentage of Branches
      int getPercentCacheMiss(void);  // Get Number of Percentage of Cache Misses
			Task();				 				 	 	      // Task constructor

		private:
      int weight;              // Task's weight
			int type;			           // Type of Task (Depending on the type, this task will be faster on a device of the same type)
      int nLoads;              // Number of Loads
      int nStores;             // Number of Stores
      int nInst;               // Number of Instructions
      int percentBranch;       // Percentage of Branches
      int percentCacheMiss;    // Percentage of Cache Misses
};

// Task constructor - Define its weight
Task::Task(void)
{
  weight	            = rand() % 2000 + 500; 	//  Task weight - Random Value 2000-2500
  nLoads              = rand() % 1000 + 250; 	//  Numer of Loads - Random Value 1000-1250
  nStores             = rand() % 700 + 300; 	//  Numer of Stores - Random Value 700-1000
  nInst               = rand() % 1000 + 250; 	//  Numer of Instructions - Random Value 1000-250
  percentBranch       = rand() % 70 + 30;     //  Percent of Branches - Random Value 10-29
  percentCacheMiss    = rand() % 10 + 80;        	//  Percentage of Cache Misses - 10-90
  type                = rand() % 2;         	//  Task weight - Random Value 0-2
}

// Set task's info
void Task::setTask(void){
  weight	            = rand() % 2000 + 500; 	//  Task weight - Random Value 2000-2500
  nLoads              = rand() % 1000 + 250; 	//  Numer of Loads - Random Value 1000-1250
  nStores             = rand() % 700 + 300; 	//  Numer of Stores - Random Value 700-1000
  nInst               = rand() % 1000 + 250; 	//  Numer of Instructions - Random Value 1000-250
  percentBranch       = rand() % 70 + 30;     //  Percent of Branches - Random Value 10-29
  percentCacheMiss    = rand() % 10 + 80;     //  Percentage of Cache Misses - 10-90
  type                = rand() % 2;         	//  Task weight - Random Value 0-2
}

// Get task's weight
int Task::getWeight(void){
	return weight;
}

// Get task's type
int Task::getType(void){
	return type;
}

// Get task's nStores
int Task::getNLoads(void){
	return nLoads;
}

// Get task's nStores
int Task::getNStores(void){
	return nStores;
}

// Get task's nInst
int Task::getNInst(void){
	return nInst;
}

// Get percentage of Branch
int Task::getPercentBranch(void){
  return percentBranch;
}

// Get task's percentCacheMiss
int Task::getPercentCacheMiss(void){
	return percentCacheMiss;
}
