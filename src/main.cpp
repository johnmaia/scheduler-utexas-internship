#include <iostream>
#include <vector>
#include "Worker.h"
#include "Scheduler.h"

#define NUMBEROFWORKERS 3
#define CPU 0
#define GPU 1
#define MIC 2

using namespace std;

int main(int argc, char** argv){
	// Initiate scheduler
	Scheduler scheduler;

	//Define workers
	typedef std::vector<AbstractWorker*> AbstractWorkerVector;
	AbstractWorkerVector workers;
 	workers.push_back(new Worker<CPU>());
 	workers.push_back(new Worker<GPU>());
 	workers.push_back(new Worker<MIC>());

	//Start working
	cout << "Scheduler starting with: " << NUMBEROFWORKERS << " workers" << endl;
	scheduler.schedule(workers, 9000, NUMBEROFWORKERS);
	return 1;
}
