#include <stdio.h>
#include <cstring>
#include <vector>
#include <cassert>
#include "Task.h"

using namespace std;

class BlockOfTasks
{
   public:
		 	void setBlock(int size);		                                          // Set block Size and initializa all the tasks
      void setTask(Task tsk);		                                            // Set task in  the vector
      void freeBlock(void);		                                              // Free block
      void createSubBlockTask(BlockOfTasks* bT, int initPoint, int ntasks); // Creates a sub block from another block
      Task getTask(int taskID);                                             // Returns a task
      int getTaskWeight(int taskID);                                        // Get weight from a certain task within the block
      int getTaskType(int taskID);                                          // Get type from a certain task within the block
      int getTaskNLoads(int taskID);                                        // Get number of loads from a certain task within the block
      int getTaskNStores(int taskID);                                       // Get number of stores from a certain task within the block
      int getTaskNInst(int taskID);                                         // Get number of Instructions from a certain task within the block
      int getTaskPercentBranch(int taskID);                                 // Get NBranchs from a certain task within the block
      int getTaskPercentCacheMiss(int taskID);                              // Get percentage of cache misses from a certain task within the block
      int getBlockSize(void);                                               // Get block size
      BlockOfTasks() : tasks() {};                                          // BlockOfTasks Constructor
		private:
      std::vector<Task> tasks;   // Vector of Tasks
};

// Set block Size and initializa all the tasks
void BlockOfTasks::setBlock(int size)
{
    for(unsigned int i = 0; i < size; ++i){
      Task tskAux;
      tasks.push_back(tskAux);
    }
}

void BlockOfTasks::setTask(Task tsk){
  //cout << "[setTask] Setting" << endl;
  tasks.push_back(tsk);
}

// Free block and all the tasks
void BlockOfTasks::freeBlock(void){
  tasks.clear();
}

// Creates a sub block from another block
void BlockOfTasks::createSubBlockTask(BlockOfTasks* bT, int initPoint, int ntasks)
{
    int btSize = bT->getBlockSize();
    int nTasksToCopy = 0;
    int endPoint = 0;

    if(btSize < (initPoint + ntasks)){
       nTasksToCopy = btSize - initPoint;
       endPoint = btSize;
     }
    else{
      nTasksToCopy = ntasks;
      endPoint = initPoint + ntasks;
    }

    //cout << "initPoint: " << initPoint << " | endPoint: " << endPoint << endl;
    btSize = nTasksToCopy;

    for(unsigned int i = initPoint; i < endPoint && i < btSize; ++i){
      //cout << "[CREATSUB] Push: " << i << " | bT.getTask(i).getWeight(): " << bT.getTask(i).getWeight() << endl;
      setTask(bT->getTask(i));
   }
}

// Get block size
int BlockOfTasks::getBlockSize(void)
{
		return tasks.size();
}

// Returns a task
Task BlockOfTasks::getTask(int taskID)
{
		return tasks[taskID];
}

// Get weight from a certain task within the block
int BlockOfTasks::getTaskWeight(int taskID)
{
    //cout << "TaskID: " << taskID << " | TaskSize: " << tasks.size() << endl;
    assert(taskID < tasks.size());
		return tasks[taskID].getWeight();
}

// Get type from a certain task within the block
int BlockOfTasks::getTaskType(int taskID)
{
		return tasks[taskID].getType();
}

// Get NLoads from a certain task within the block
int BlockOfTasks::getTaskNLoads(int taskID)
{
		return tasks[taskID].getNLoads();
}

// Get NStores from a certain task within the block
int BlockOfTasks::getTaskNStores(int taskID)
{
		return tasks[taskID].getNStores();
}

// Get NInst from a certain task within the block
int BlockOfTasks::getTaskNInst(int taskID)
{
		return tasks[taskID].getNInst();
}

// Get NBranchs from a certain task within the block
int BlockOfTasks::getTaskPercentBranch(int taskID)
{
		return tasks[taskID].getPercentBranch();
}

// Get Percent Cache Miss from a certain task within the block
int BlockOfTasks::getTaskPercentCacheMiss(int taskID)
{
		return tasks[taskID].getPercentCacheMiss();
}
