#include <iostream>
#include <fstream>
#include <vector>
#include "Timer.h"

using namespace std;

class Scheduler
{
   public:
		 	void initOutputFile(void);                                                            // Init output file
      void closeOutputFile(void);                                                           // Close output file
      void setStablePercent(int stPercent);                                                 // Set stable percentage
      void setInstablePercent(int istPercent);                                              // Set instable percentage
      void setNOfStepsToDouble(int nsteps);                                                 // Set number of steps to double (stable)
      void setNOfStepsToCut(int nsteps);                                                    // Set number of steps to cut (instable)
      void schedule(std::vector<AbstractWorker*> &workers, int BlckSize, int nWorkers);     // Schedule
      Scheduler();                                                                          // Scheduler Constructor
		private:
			ofstream outputFile;		    // Scheduler OutputFile Pointer
    	int stablePercent;					// How stable must all works be (percentage)
    	int instablePercent;				// How instable must all works be (percentage)
    	int instableStep;						// Number of times that all workers have been instable
    	int stableStep;							// Number of times that all workers have been stable
    	int percentStep;					 	// Average percentage of wait time of all workers
    	int numberOfStepsToDouble; 	// Number of stable steps before doubling the task size
    	int numberOfStepsToCut; 		// Number of stable steps before doubling the task size
    	int totalWorkSize;

      int* wrkTaskSize;	                // Array that stores the size of task for each worker
      int* wrkWaitTimePercentage;       // Array that stores the percentage of waiting time for each worker
      float* wrkWorkloadPercentage;     // Array that stores the percentage of workload of each worker
};

// Scheduler Constructor
Scheduler::Scheduler(void)
{
  initOutputFile();
  stablePercent = 5;
  instablePercent = 20;
  instableStep = 0;
  stableStep = 0;
  percentStep = 0;
  numberOfStepsToDouble = 3;
  numberOfStepsToCut = 1;
  totalWorkSize = 0;
}

// Init output file
void Scheduler::initOutputFile(void)
{
  outputFile.open("misc/output.dat");
  outputFile << "# \t W1 \t W2 \t W3 \t SP \t TM" << endl;
}

// Close output file
void Scheduler::closeOutputFile(void)
{
  outputFile.close();
}

// Set stable percentage
void Scheduler::setStablePercent(int stPercent)
{
  stablePercent = stPercent;
}

// Set instable percentage
void Scheduler::setInstablePercent(int istPercent)
{
  instablePercent = istPercent;
}

// Set number of steps to double (stable)
void Scheduler::setNOfStepsToDouble(int nsteps)
{
  numberOfStepsToDouble = nsteps;
}

// Set number of steps to cut (instable)
void Scheduler::setNOfStepsToCut(int nsteps)
{
  numberOfStepsToCut = nsteps;
}

// Sechdule
void Scheduler::schedule(std::vector<AbstractWorker*> &workers, int BlckSize, int nWorkers)
{
  double start, finish, elapsed;								                             // Aux variables to calculte elapsed time
  int totalWorkDone = 0;
  int totalWorkNextIter = 0;
  int totalWorkNextIterAux = 0;
  int lastStartPoint = 0;
  int iter = 0;
  float sumNWL = 0.f;
  wrkTaskSize = (int*) malloc(sizeof(int*) * nWorkers);                       // Aux variables to save the actual each worker task size
  wrkWaitTimePercentage = (int*) malloc(sizeof(int*) * nWorkers);             // Aux variables to save the actual each worker total waiting
  wrkWorkloadPercentage = (float*) malloc(sizeof(float*) * nWorkers);         // Array that stores the percentage of workload of each worker
  BlockOfTasks *wrkBlockOfTasks = new BlockOfTasks[nWorkers];                 //right

  // Init tasks and set work to each worker
	for(unsigned int i = 0; i < nWorkers; ++i){
		wrkTaskSize[i] = BlckSize / (100 * nWorkers);
		wrkBlockOfTasks[i].setBlock(wrkTaskSize[i]);
    wrkWorkloadPercentage[i] = 100/nWorkers;

		workers[i]->setBlockOfTasks(&wrkBlockOfTasks[i]);
    lastStartPoint += wrkTaskSize[i];
    totalWorkNextIter = lastStartPoint;
	}

  for(unsigned int i = 0; i < nWorkers; ++i){
    cout << "[i] Iter " << iter << " | Thread " << i << " task size: " << wrkTaskSize[i] << endl;
  }

  // Initiate workers and feed them with work
  while(lastStartPoint <= BlckSize){
    double start, finish, elapsed;

    //Reset Variables
    totalWorkSize = 0;
    percentStep = 0;
    sumNWL = 0.f;
    totalWorkNextIterAux = 0;

    GET_TIME(start);

    // Start workers
    for(unsigned int i = 0; i < nWorkers; ++i) workers[i]->startWorker();

    // Join workers
    for(unsigned int i = 0; i < nWorkers; ++i) workers[i]->joinWorker();

    GET_TIME(finish);

    // Calculate elapsed time
    elapsed = finish - start;

    // Get elapsed time from each workers and calculate their waiting time
    for(unsigned int i = 0; i < nWorkers; ++i){
        wrkWaitTimePercentage[i] = ((elapsed - workers[i]->getElapsedTime())/elapsed)*100;
        totalWorkSize += workers[i]->getBlockOfTasksSize();
        cout << "[+][Thread: " << i << "] waited " << wrkWaitTimePercentage[i] << " percent of the time (" << workers[i]->getBlockOfTasksSize() << " tasks)" << endl;
    }
    cout << "[+] Elapsed time: " << elapsed << " s " << endl;
    cout << "[+] Speed: " << (float) totalWorkSize/elapsed << " T/s" << endl;
    cout << "[+] Remaining tasks: " << BlckSize - lastStartPoint << endl;

    if(lastStartPoint < BlckSize){
      // Compute new task sizes for each thread and the total waiting percentage
      for(unsigned int i = 0; i < nWorkers; ++i){
        percentStep += wrkWaitTimePercentage[i];
      }

      // Calculate the Waiting Average and compute the Stable check variables
      // percentStep /= nWorkers;
      // if(percentStep <= stablePercent) ++stableStep;
      // else if(percentStep >= instablePercent) ++instableStep;
      // else {
      //   stableStep = 0;
      //   instableStep = 0;
      // }
      // Check if the number of steps till double the size has been reached
      // if(stableStep == numberOfStepsToDouble){
      //   cout << "[!] Stability has been reached, going to double the task size!" << endl;
      //   stableStep = 0;
      //
      //   // Compute new task sizes for each thread
      //   for(unsigned int i = 0; i < nWorkers; ++i) wrkTaskSize[i] *= 2;
      // } else
      // if (instableStep == numberOfStepsToCut){
      //   for(unsigned int i = 0; i < nWorkers; ++i){
      //     if(!wrkWaitTimePercentage[i]){
      //       cout << "[!] Instable! Cutting worker " << i << " task size!" << endl;
      //       //wrkTaskSize[i] *= (double) wrkWaitTimePercentage[i]/100;
      //       instableStep = 0;
      //     }
      //   }
      // } else {
      //   // Compute new task sizes for each thread and the total waiting percentage
      //   for(unsigned int i = 0; i < nWorkers; ++i){
      //       // Increase tasks in order to equail the time of the slowest
      //       if(wrkWaitTimePercentage[i]){
      //         wrkTaskSize[i] *= (1 + ((double) wrkWaitTimePercentage[i]/100));
      //       }
      //   }
      // }

      outputFile <<  iter << " \t ";
      for(unsigned int i = 0; i < nWorkers; ++i)  outputFile << (float) wrkWorkloadPercentage[i] << " \t ";
      outputFile <<  (double) totalWorkSize/elapsed << " \t " << elapsed << endl;

      ++iter;

      // for(unsigned int i = 0; i < nWorkers; ++i){
      //     if(wrkWaitTimePercentage[i]){
      //     wrkWorkloadPercentage[i] = wrkWorkloadPercentage[i] * (1 + ((float) wrkWaitTimePercentage[i]/(100 * (nWorkers - 1))));
      //     cout << "[WB] Worker : " << i << " | wrkWorkloadPercentage[i]: " << wrkWorkloadPercentage[i] << endl;
      //     sumWLPNN += ((float) wrkWaitTimePercentage[i]/(100 * (nWorkers - 1)));
      //     nzeros--;
      //     }
      // }
      //
      // // Deal with workers that didn't wait any time
      // for(unsigned int i = 0; i < nWorkers; ++i){
      //     if(!wrkWaitTimePercentage[i]){
      //     wrkWorkloadPercentage[i] = wrkWorkloadPercentage[i] * (1 - ((float) sumWLPNN/nzeros));
      //     }
      // }
      //
      // if((lastStartPoint+totalWorkNextIter) > BlckSize) totalWorkNextIter = BlckSize - lastStartPoint;

      // Calculate new workload
      for(unsigned int i = 0; i < nWorkers; ++i){
        wrkWorkloadPercentage[i] = wrkWorkloadPercentage[i] + (wrkWaitTimePercentage[i] * (wrkWorkloadPercentage[i]/100.f));
        sumNWL += wrkWorkloadPercentage[i];
      }

      // Normalize
      for(unsigned int i = 0; i < nWorkers; ++i){
        wrkWorkloadPercentage[i] /= sumNWL;
        wrkWorkloadPercentage[i] *= 100.f;
      }

      // Atribute tasks to each worker based on their workload
      for(unsigned int i = 0; i < nWorkers; ++i){
        wrkTaskSize[i] = totalWorkNextIter * ((float) wrkWorkloadPercentage[i]/100.f);
        totalWorkNextIterAux += wrkTaskSize[i];
      }

      // Atribute the extra tasks
      while(totalWorkNextIterAux < totalWorkNextIter){
        for(unsigned int i = 0; i < nWorkers && (totalWorkNextIterAux < totalWorkNextIter); ++i){
          wrkTaskSize[i] += 1;
          ++totalWorkNextIterAux;
        }
      }

      // Calculate workload after extras
      for(unsigned int i = 0; i < nWorkers; ++i){
        wrkWorkloadPercentage[i] = ((float) wrkTaskSize[i]/ (float) totalWorkNextIter)*100.f;
      }

      // Atribute each block to each thread
      for(unsigned int i = 0; i < nWorkers; ++i){
        wrkBlockOfTasks[i].freeBlock();
        wrkBlockOfTasks[i].setBlock(wrkTaskSize[i]);

        workers[i]->freeBlockOfTasks();
        workers[i]->setBlockOfTasks(&wrkBlockOfTasks[i]);
        lastStartPoint += wrkTaskSize[i];
      }

      // Print info related to the next Iteration
      for(unsigned int i = 0; i < nWorkers; ++i){
        cout << "[i] Iter " << iter << " | Thread " << i << " task size: " << wrkTaskSize[i] << endl;
      }
    } else {
      // Outpu the log file the info related to the last Iteration
      outputFile <<  iter << " \t ";
      for(unsigned int i = 0; i < nWorkers; ++i)  outputFile << (double) wrkWorkloadPercentage[i] << " \t ";
      outputFile <<  (double) totalWorkSize/elapsed << " \t " << elapsed << endl;
      ++lastStartPoint;
    }
  }
  // Free used variables
  for(unsigned int i = 0; i < nWorkers; ++i){
    wrkBlockOfTasks[i].freeBlock();
    workers[i]->freeBlockOfTasks();
  }
  delete [] wrkBlockOfTasks;
}
