gnuplot <<_EOF_
set term png
set output "speed.png"
set ylabel "Speed (#Tasks/s)"
set xlabel "#Iterations"
plot 'output.dat' using 1:5 title "Speed"  with lines
_EOF_

gnuplot <<_EOF_
set term png
set output "time.png"
set ylabel "Time (s)"
set xlabel "#Iterations"
plot 'output.dat' using 1:6 title "Time"  with lines
_EOF_

gnuplot <<_EOF_
set term png
set output "workload.png"
set ylabel "Work (#Tasks)"
set xlabel "#Iterations"
plot 'output.dat' using 1:2 title "Worker 1"  with lines, 'output.dat' using 1:3 title "Worker 2"  with lines, 'output.dat' using 1:4 title "Worker 3"  with lines
_EOF_
