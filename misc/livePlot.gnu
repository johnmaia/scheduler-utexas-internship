set term aqua 1 size 425 325 title 'Speed'
set ylabel "Speed (#Tasks/s)"
set xlabel "#Iterations"
plot 'output.dat' using 1:5 title "Speed"  with lines

set term aqua 2 size 425,325 title 'Time'
set ylabel "Time (s)"
set xlabel "#Iterations"
plot 'output.dat' using 1:6 title "Time"  with lines

set term aqua 3 size 425,325 title 'Work balance'
set ylabel "Work (#Tasks)"
set xlabel "#Iterations"
plot 'output.dat' using 1:2 title "Worker 1"  with lines, 'output.dat' using 1:3 title "Worker 2"  with lines, 'output.dat' using 1:4 title "Worker 3"  with lines
pause 5
reread
